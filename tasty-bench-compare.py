#!/usr/bin/env python
# -*- coding: utf-8 -*-

import pandas as pd
import io

def read_results(name: str, f) -> pd.DataFrame:
    x =  pd.read_csv(f, usecols=[0,1,2])
    y = x.groupby('Name').first()
    y.columns = pd.MultiIndex.from_tuples([(name, 'mean'), (name, 'stdev')], names=['file', 'what'])
    return y

def main() -> None:
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument('reference', metavar='CSV', type=argparse.FileType('rb'))
    parser.add_argument('other', metavar='CSV', nargs='+', type=argparse.FileType('rb'))
    args = parser.parse_args()

    ref = read_results('ref', args.reference)
    others = pd.concat([ read_results(f.name, f) for f in args.other ], axis=1)
    d = pd.concat([ref, others], axis=1)

    for i in others.columns.levels[0]:
        d[(i, 'rel')] = (others[i, 'mean'] / d['ref', 'mean'])
        n = len(others[i, 'mean'])
        print(i, d[(i, 'rel')].product()**(1/n))

    #print(d.to_string())
    print(d.stack().to_markdown())

if __name__ == '__main__':
    main()
