#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
A tool for uploading GHC's core libraries to Hackage.

This is a utility for preparing and uploading source distributions and
documentation of GHC's core libraries to Hackage. This should be run in
a GHC tree of the release commit after having run ./configure.
"""

from subprocess import run, check_call
import shutil
from pathlib import Path
from typing import NamedTuple, Callable, List, Dict, Optional
import tempfile
import re

STAMPS = Path('.upload-libs')
STAMPS.mkdir(exist_ok=True)

class Package(NamedTuple):
    name: str
    path: Path
    prepare_sdist: Callable[[], None]

def no_prep():
    pass

def prep_base():
    shutil.copy('config.guess', 'libraries/base')
    shutil.copy('config.sub', 'libraries/base')

def build_copy_file(pkg: Package, f: Path):
    target = Path('_build') / 'stage1' / pkg.path / 'build' / f
    dest = pkg.path / f
    print(f'Building {target} for {dest}...')

    run(['hadrian/build-cabal', target], check=True)
    dest.parent.mkdir(exist_ok=True, parents=True)
    shutil.copyfile(target, dest)

def prep_ghc_prim():
    build_copy_file(PACKAGES['ghc-prim'], Path('GHC/PrimopWrappers.hs'))

def prep_ghc_bignum():
    shutil.copy('config.guess', 'libraries/base')
    shutil.copy('config.sub', 'libraries/base')

def prep_ghc_boot():
    build_copy_file(PACKAGES['ghc-boot'], Path('GHC/Platform/Host.hs'))
    build_copy_file(PACKAGES['ghc-boot'], Path('GHC/Version.hs'))

PACKAGES = {
    pkg.name: pkg
    for pkg in [
        Package('base', Path("libraries/base"), prep_base),
        Package('ghc-prim', Path("libraries/ghc-prim"), prep_ghc_prim),
        Package('integer-gmp', Path("libraries/integer-gmp"), no_prep),
        Package('ghc-bignum', Path("libraries/ghc-bignum"), prep_ghc_bignum),
        Package('template-haskell', Path("libraries/template-haskell"), no_prep),
        Package('ghc-heap', Path("libraries/ghc-heap"), no_prep),
        Package('ghc-boot', Path("libraries/ghc-boot"), prep_ghc_boot),
        Package('ghc-boot-th', Path("libraries/ghc-boot-th"), no_prep),
        Package('ghc-compact', Path("libraries/ghc-compact"), no_prep),
        Package('libiserv', Path("libraries/libiserv"), no_prep),
        Package('ghc', Path("compiler"), no_prep),
    ]
}
# Dict[str, Package]

def cabal_upload(tarball: Path, publish: bool=False, extra_args=[]):
    if publish:
        extra_args += ['--publish']

    run(['cabal', 'upload'] + extra_args + [tarball], check=True)

def upload_sdist(pkg: Package):
    stamp = STAMPS / f'{pkg.name}-sdist'
    if stamp.is_file():
        return

    print(f'Uploading package {pkg.name}...')
    shutil.rmtree(pkg.path / 'dist-newstyle', ignore_errors=True)
    pkg.prepare_sdist()
    
    # Upload source tarball
    run(['cabal', 'sdist'], cwd=pkg.path, check=True)
    sdist = list((pkg.path / 'dist-newstyle' / 'sdist').glob('*.tar.gz'))[0]
    cabal_upload(sdist)

    print()
    print('Check over candidate on Hackage and press enter when ready...')
    input()
    cabal_upload(sdist, publish=True)
    stamp.write_text('')

def get_version(cabal_file: Path) -> Optional[str]:
    m = re.search(r'^version:\s*(\d+(\.\d+)*)', cabal_file.read_text(), re.I | re.M)
    return None if m is None else m.group(1)

def upload_docs(bindist: Path, pkg: Package):
    """
    Upload Haddock documentation for a package. bindist
    is the path to an extract binary distribution produced by
    make.
    """
    stamp = STAMPS / f'{pkg.name}-docs'
    if stamp.is_file():
        return

    docdir = bindist / pkg.path / 'dist-install' / 'doc' / 'html' / pkg.name
    cabal_file = pkg.path / f'{pkg.name}.cabal'
    version = get_version(cabal_file)
    assert version is not None

    # Build the documentation tarball from the bindist documentation
    stem = f'{pkg.name}-{version}-docs'
    tmp = tempfile.TemporaryDirectory(stem)
    shutil.copytree(docdir, Path(tmp.name) / stem)
    tarball = Path(f'{stem}.tar.gz')
    run(['tar', '-czf', tarball, '-H', 'ustar', '-C', tmp.name, stem])
    
    # Upload the documentation tarball
    print(f'Uploading documentation for {pkg.name}-{version}...')
    cabal_upload(tarball, publish=True, extra_args=['--documentation'])
    stamp.write_text('')

def upload_pkg(bindist: Path, pkg: Package):
    print(f'Processing {pkg.name}...')
    upload_sdist(pkg)
    upload_docs(bindist, pkg)

def upload_all(bindist: Path):
    for pkg in PACKAGES.values():
        upload_pkg(bindist, pkg)

def main() -> None:
    import argparse

    parser = argparse.ArgumentParser(description=__doc__)
    parser.add_argument('--bindist', type=Path, help='extracted binary distribution')
    parser.add_argument('pkg', type=str, nargs='*', help='package to upload')
    args = parser.parse_args()

    pkgs = args.pkg
    if pkgs == []:
        pkgs = PACKAGES.keys()

    for pkg_name in pkgs:
        assert pkg_name in PACKAGES

    for pkg_name in pkgs:
        print(pkg_name)
        pkg = PACKAGES[pkg_name]
        upload_pkg(args.bindist, pkg)

if __name__ == '__main__':
    main()

