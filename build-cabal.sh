#!/usr/bin/env bash

set -e -x
out=_cabal_out
dir=libraries/Cabal/Cabal
if [[ -z "$GHC" ]]; then
  GHC="_build/stage1/bin/ghc"
fi
if [[ -z "$WRAPPER" ]]; then
  WRAPPER="time"
fi

rm -Rf $out
export GHC_ENVIRONMENT="-"
exec $WRAPPER $GHC -package parsec -XHaskell2010 -hidir $out -odir $out -i$dir -i$dir/src $dir/Setup.hs +RTS -s -RTS $@
