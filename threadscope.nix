{ pkgs ? (import <nixpkgs> {}) }:

let
  ghc-events = 
    let
      src = fetchGit {
        url = "https://github.com/bgamari/ghc-events";
        rev = "3d05b46547f777d90765bc03b81cc10fd49f6816";
        ref = "ticky";
      };
    in pkgs.haskellPackages.callCabal2nix "ghc-events" src {};

  threadscope = 
    let
      src = fetchGit {
        url = "https://github.com/bgamari/threadscope";
        rev = "517eb7b631e9987997f413a97e9b766510aab34c";
      };
    in pkgs.haskell.lib.doJailbreak (pkgs.haskellPackages.callCabal2nix "threadscope" src { inherit ghc-events; });
in threadscope
