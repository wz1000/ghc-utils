-- tickets opened per year
select date_part('year', created_at) as year, count(*) as opened_tickets
from issues
group by year
order by year;

-- tickets closed per year
select date_part('year', closed_at) as year, count(*) as closed_tickets
from issues
group by year
order by year;

-- open tickets over time
-- TODO
